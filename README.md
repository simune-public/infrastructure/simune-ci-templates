CI templates for Simune
=======================

This repository provides CI templates and scripts that can be used by all
Python-based repositories within Simune, for building, testing and packaging
purposes.

> :warning: **CI pipelines triggered by users who do not have access to this
> repository will systematically fail.**


Using templates in your repositories
------------------------------------

The templates available here can be included in your repositories by adding
the following at the beginning of your corresponding `.gitlab-ci.yml` files:

```yaml
include:
  - project: simune-public/infrastructure/simune-ci-templates
    ref: master
    file: /TEMPLATE.yml
```

replacing `TEMPLATE` by the name of the template you would like to use.
